from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf

import os

def myNetwork():
   os.system('mn -c')
   net = Mininet( link=TCLink,
                  host=CPULimitedHost )

   h1 = net.addHost( 'server', ip='10.0.0.2/24', mac='00:00:00:00:00:01' )
   h2 = net.addHost( 'client', ip='10.0.1.2/24', mac='00:00:00:00:00:02' )

   r1 = net.addHost('r1')

   net.addLink(r1,h1,bw=2,max_queue_size=60)
   net.addLink(r1,h2,bw=1000,max_queue_size=60)
   net.build()

   r1.cmd('ifconfig r1-eth0 0')
   r1.cmd('ifconfig r1-eth1 0')

   r1.cmd('ifconfig r1-eth0 hw ether 00:00:00:00:01:01')
   r1.cmd('ifconfig r1-eth1 hw ether 00:00:00:00:01:02')

   r1.cmd('ip addr add 10.0.0.1/24 brd + dev r1-eth0')
   r1.cmd('ip addr add 10.0.1.1/24 brd + dev r1-eth1')
   r1.cmd('sysctl -w net.ipv4.ip_forward=1')

   h1.cmd('ip route add default via 10.0.0.1')
   h2.cmd('ip route add default via 10.0.1.1')

   h1.cmd('sysctl -w net.ipv4.tcp_congestion_control=reno')
   h2.cmd('sysctl -w net.ipv4.tcp_congestion_control=reno')

   h1.cmd('python -m SimpleHTTPServer 80 &')
   #h1.cmd('modprobe tcp_probe port=80 full=1')

   print('** Start Mininet **')
   CLI(net)
   print('** Stop Mininet **')
   net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()

