import time 
import os
  
def rtt(url): 
    t1 = time.time()
    os.system('ping -c 20 {}'.format(url))
    t2 = time.time() 
    tim = str(t2-t1) 
    print("Time in seconds :" + tim) 
  

url = "10.0.0.2"
ans = 0
for i in range(10):
	t = rtt(url)
	print "Average RTT #{}: {}".format(i + 1, t)
	ans += t
print "Average RTT total: {}".format(ans/10)